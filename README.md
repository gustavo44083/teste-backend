## About
This is a mockup API built to manage movie titles and users for IMDB.
It was build as [a back-end test](https://bitbucket.org/ioasys/teste-backend) for [ioasys](https://www.ioasys.com.br/).


### Built with
- [NodeJS](https://nodejs.org/en/)
- [Typescript](https://www.typescriptlang.org/)
- [Typegoose](https://github.com/typegoose/typegoose)
- [Passport](http://www.passportjs.org/)
- [class-transformer](https://github.com/typestack/class-transformer) and [class-validator](https://github.com/typestack/class-validator)


## Getting Started

### Prerequisites
- NodeJS 10.0+
- MongoDB

### Instalation
1. Clone the repo
    ```sh
    git clone https://bitbucket.org/gustavo44083/teste-backend.git
    ```
2. Install NPM packages
    ```sh
    npm install
3. Create your `.env` file (you can use `example.env` as an example)
4. Start a MongoDB database and enter its URI in the `.env` file
5. Run `npm start` to start the API

The first admin access will be created and should appear after the server starts.

### Docker
To run this project in Docker, you can use `docker-compose up` our build an image and run on your Docker environment using
`docker build`.


## Usage

This API has been documented using Postman. For more information on how to use it and examples, please refer to
the [API Documentation](https://documenter.getpostman.com/view/1217340/TVt2dPsy).

### CORS
You can change the CORS options using the `APP_CORS_OPTIONS` environment variable. Refer to
[CORS Configuration Options](https://www.npmjs.com/package/cors#configuration-options) for more information.


## Contact
Gustavo Ferreira - gustavo44083@gmail.com
