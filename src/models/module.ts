import Controller from "./controller";

export default interface Module {
  controllers: Controller[]
}
