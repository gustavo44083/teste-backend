export class RecordNotFoundError extends Error {
  constructor(message: string) {
    super(message);
  }
}

export class DuplicatedKeyError extends Error {
  constructor(message: string) {
    super(message)
  }
}
