import * as dotenv from "dotenv"
import { resolve } from "path"

const path = resolve(__dirname, `../${process.env.NODE_ENV || ''}.env`);
dotenv.config({path: path});
