import "../lib"
import { mongoose } from "@typegoose/typegoose";
import { SETTINGS } from "../config";

beforeAll(async () => {
  await mongoose.connect(SETTINGS.MONGODB_URI, {useNewUrlParser: true, useUnifiedTopology: true});
})

afterEach(async () => {
  for (let collection in mongoose.connection.collections) {
    await mongoose.connection.db.dropCollection(collection)
  }
})
