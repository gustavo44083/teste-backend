import cors, { CorsOptions } from 'cors'
import express, { RequestHandler } from "express";
import { Express } from "express";
import helmet from "helmet";
import passport from "passport";
import Module from "./models/module";
import bodyParser from "body-parser";

interface AppOptions {
  cors?: CorsOptions
}

export default class App {
  server: Express

  constructor(private modules: Module[]) {
    this.server = express()
  }

  init(options: AppOptions): void {
    this.server.use(helmet())
    this.server.use(cors(options.cors))
    this.server.use(passport.initialize())
    this.server.use(bodyParser.json())
    this.modules.forEach(m => m.controllers.forEach(c => {
      const middlewares: RequestHandler[] = []
      if (c.authenticated) {
        middlewares.push(passport.authenticate('jwt', {session: false}))
      }

      this.server.use(c.path, ...middlewares, c.router)
    }))
  }

  listen(port: number, callback: () => void): void {
    this.server.listen(port, callback)
  }
}
