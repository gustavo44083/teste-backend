import { NextFunction, Request, Response } from "express";
import { RecordNotFoundError } from "./models/exceptions";
import { User } from "./modules/user/user.model";

export const customToJson = (doc: any, ret: any) => {
  return {id: ret._id, ...ret, _id: undefined, __v: undefined}
}

export const checkRole = (role: string) => (request: Request, response: Response, next: NextFunction) => {
  if(!request.user) {
    return next(new Error("Invalid user"))
  }

  const user = request.user as User
  if(user.roles && user.roles.includes(role)) {
    return next()
  } else {
    return response.status(403).json({error: "You don't have the required roles to perform this action"})
  }
}

/**
 * Checks the result of an update operation on the database.
 *
 * @param result MongoDB update result
 * @param errorMessage Error message that will be used if nothing is found
 * @throws {@link RecordNotFoundError} if no record was found on the database
 * @returns `true` if something was modified, `false` otherwise
 */
export function validateDatabaseUpdate(result: any, errorMessage: string): boolean {
  if(result['n'] == 0) {
    throw new RecordNotFoundError(errorMessage)
  }

  return result['nModified'] > 0
}

export const defaultModelOptions = {
  schemaOptions: {toJSON: {transform: customToJson}}
}

export const notDeleted = {deleted: {$ne: true}}
