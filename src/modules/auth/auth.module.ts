import * as bcrypt from "bcrypt";
import passport from "passport";
import { BasicStrategy } from "passport-http";
import { ExtractJwt, Strategy as JwtStrategy } from "passport-jwt";
import Controller from "../../models/controller";
import Module from "../../models/module";
import { notDeleted } from "../../util";
import { UserModel } from "../user/user.model";
import AuthController from "./auth.controller";


export default class AuthModule implements Module {
  controllers: Controller[];

  constructor() {
    const privateKey = this.loadPrivateKey();
    const jwtOptions = {
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      secretOrKey: privateKey
    };

    // JWT authentication
    passport.use(new JwtStrategy(jwtOptions, async (jwtPayload, done) => {
      try {
        const user = await UserModel.findOne({email: jwtPayload.sub, ...notDeleted})
        if (user && !this.isExpired(jwtPayload)) {
          return done(null, user)
        } else {
          return done(null, false);
        }
      } catch (e) {
        return done(e, false)
      }
    }));

    // Basic authentication used to create a JWT
    passport.use(new BasicStrategy(async (email, password, done) => {
      try {
        const user = await UserModel.findOne({email, ...notDeleted}, "email roles password")
        if (user) {
          const validPassword = await bcrypt.compare(password, user.password)
          if (validPassword) {
            return done(null, user)
          }
        }

        return done(null, false)
      } catch (e) {
        return done(e)
      }
    }));

    this.controllers = [
      new AuthController(privateKey)
    ]
  }

  /**
   * Loads a private key from the environment variable APP_SECRET.
   *
   * @returns The private key defined on the environment variable APP_SECRET or `secret` by default
   */
  private loadPrivateKey(): string {
    const secret = process.env.APP_SECRET;
    if (!secret) {
      console.warn('No secret key have been defined. Please use the environment variable APP_SECRET to define one.')
    }

    return secret || 'secret'
  }

  /**
   * Checks if a JWT token has expired.
   *
   * @param jwtPayload The JWT's payload
   * @returns `true` if the token is expired
   */
  private isExpired(jwtPayload: any): boolean {
    return Math.floor(Date.now() / 1000) > jwtPayload.exp
  }
}
