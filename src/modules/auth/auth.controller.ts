import { Request, Response, Router } from "express";
import * as jwt from "jsonwebtoken";
import passport from "passport";
import Controller from "../../models/controller";
import { User } from "../user/user.model";


export default class AuthController implements Controller {
  path = '/authenticate'
  router = Router();
  authenticated = false;

  constructor(private secret: string) {
    this.router.post('/', passport.authenticate('basic', {session: false}), this.authenticate)
  }

  /**
   * Authenticate a new JWT for an user.
   */
  private authenticate = async (request: Request, response: Response) => {
    const exp = new Date()
    exp.setDate(exp.getDate() + 1)

    const signedJwt = jwt.sign({
      sub: (request.user as User).email,
      exp: Math.floor(exp.getTime() / 1000)
    }, this.secret);

    response.json({
      token: signedJwt,
      type: 'bearer'
    })
  }
}
