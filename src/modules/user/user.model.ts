import { getModelForClass, index, modelOptions, prop } from '@typegoose/typegoose'
import { Base } from "@typegoose/typegoose/lib/defaultClasses";
import { Exclude } from 'class-transformer'
import { IsEmail, IsString, MinLength } from "class-validator";
import { defaultModelOptions } from "../../util";

@modelOptions(defaultModelOptions)
@index({email: 1}, {unique: true})
export class User extends Base<string> {
  @prop()
  _id: string

  @prop()
  @Exclude()
  createdAt: Date

  @prop()
  @IsEmail()
  email: string

  @prop({select: false})
  @IsString()
  @MinLength(8)
  password: string

  @prop({type: String})
  roles: string[]

  @prop({select: false})
  deleted?: boolean
}

export const UserModel = getModelForClass(User)
