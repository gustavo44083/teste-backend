import { plainToClass } from 'class-transformer'
import { validate } from 'class-validator'
import { Request, Response, Router } from 'express'
import Controller from '../../models/controller'
import { DuplicatedKeyError, RecordNotFoundError } from '../../models/exceptions'
import { checkRole } from '../../util'
import { User } from './user.model'
import UserService from './user.service'

export default class UserController implements Controller {
  path = '/users'
  router = Router()
  authenticated = true

  constructor(private userService: UserService) {
    this.router.get('/me', this.getCurrentUser)
    this.router.get('/', checkRole('admin'), this.getUsers)
    this.router.post('/', checkRole('admin'), this.createUser)
    this.router.get('/:id', checkRole('admin'), this.getUser)
    this.router.put('/:id', checkRole('admin'), this.editUser)
    this.router.delete('/:id', checkRole('admin'), this.deleteUser)
  }

  /**
   * Retrieves all registered and non-deleted users.
   * Required roles: admin
   */
  private getUsers = async (request: Request, response: Response) => {
    const users = await this.userService.getUsers(
        parseInt((request.query['page'] as string) || '0'),
        parseInt((request.query['limit'] as string) || '10')
    )


    response.json({users})
  }

  /**
   * Retrieves a specific user.
   * Required roles: admin
   */
  private getUser = async (request: Request, response: Response) => {
    const user = await this.userService.getUser(request.params['id'])
    if (user) {
      response.json(user)
    } else {
      response.status(404).send()
    }
  }

  /**
   * Returns the data of the currently authenticated user.
   * Required roles: none
   */
  private getCurrentUser = async (request: Request, response: Response) => {
    response.json(request.user)
  }

  /**
   * Registers a new user. The request body should include all the user's data.
   * Required roles: admin
   */
  private createUser = async (request: Request, response: Response) => {
    const userData = plainToClass(User, request.body)
    const errors = await validate(userData)
    if (errors.length) {
      return response.status(400).json({errors})
    }

    try {
      const createdUser = await this.userService.createUser(userData)
      const asJson = createdUser.toJSON()
      delete asJson.password
      response.status(201).json(asJson)
    } catch (e) {
      if(e instanceof DuplicatedKeyError) {
        return this.handleDuplicate(response)
      }

      throw e
    }
  }

  /**
   * Updates data for an existing user. The request body should include all the user's data to be updated.
   * Required roles: admin
   */
  private editUser = async (request: Request, response: Response) => {
    const userData = plainToClass(User, request.body)
    const errors = await validate(userData)
    if (errors.length) {
      return response.status(400).json({errors})
    }

    userData._id = request.params['id']
    try {
      await this.userService.updateUser(userData)
      response.json(await this.userService.getUser(userData._id))
    } catch (e) {
      if(e instanceof DuplicatedKeyError) {
        return this.handleDuplicate(response)
      } else if (!(e instanceof RecordNotFoundError)) {
        throw e
      }

      response.status(404).send()
    }
  }

  /**
   * Deletes an user.
   * Required roles: admin
   */
  private deleteUser = async (request: Request, response: Response) => {
    try {
      const deleted = await this.userService.deleteUser(request.params['id'])
      if (deleted) {
        return response.status(200).send()
      }
    } catch (e) {
      if (!(e instanceof RecordNotFoundError))
        throw e
    }

    response.status(404).send()
  }

  /**
   * Handles the response for duplicate email addresses
   */
  private handleDuplicate(response: Response) {
    return response.status(400).json({error: 'This email address is already registered.'})
  }
}
