import { DocumentType } from '@typegoose/typegoose'
import * as bcrypt from 'bcrypt'
import { classToClass, classToPlain, plainToClass } from 'class-transformer'
import { v4 as uuidv4 } from 'uuid'
import { DuplicatedKeyError } from '../../models/exceptions'
import { notDeleted, validateDatabaseUpdate } from '../../util'
import { User, UserModel } from './user.model'

export default class UserService {
  /**
   * Creates the first admin, if none is found.
   */
  async initAdmin(): Promise<User | null> {
    const admins = await UserModel.find({...notDeleted, roles: {$in: ['admin']}})
    if (!admins.length) {
      const firstAdmin = plainToClass(User, {
        email: 'admin@example.org',
        password: 'admin',
        roles: ['admin']
      })

      await this.createUser(firstAdmin)
      return firstAdmin
    }

    return null
  }

  /**
   *
   */
  async getUsers(page: number, limit: number): Promise<User[]> {
    return UserModel.find(notDeleted).sort('-createdAt').limit(limit).skip(limit * page)
  }

  /**
   * Retrieves a user's data.
   *
   * @param id The user's ID
   */
  async getUser(id: string): Promise<DocumentType<User> | null> {
    return UserModel.findOne({_id: id, ...notDeleted})
  }

  /**
   * Creates a new user and assigns it an unique ID.
   * The user's password will be hashed before saving to the database.
   *
   * @param user The user's data
   * @returns The created user
   * @throws {@link DuplicatedKeyError} if there is another user using the same email address
   */
  async createUser(user: User): Promise<DocumentType<User>> {
    user = classToClass(user)
    user._id = uuidv4()
    user.password = await this.hashPassword(user.password)
    user.createdAt = new Date()
    try {
      return await UserModel.create(user)
    } catch (e) {
      if(e['code'] === 11000) { // duplicate key error
        throw new DuplicatedKeyError(e.message)
      }

      throw e
    }
  }

  /**
   * Updates an existing user's data. If a password is provided, it will be hashed before saving to the database.
   * The user object must have an ID.
   *
   * @param user The updated user's data
   * @returns `true` if the user has been updated
   * @throws {@link RecordNotFoundError} if no user with the specified ID is found
   */
  async updateUser(user: User): Promise<boolean> {
    if (!user._id) {
      throw new Error("User must have an ID to be updated")
    }

    user = classToClass(user)
    if (user.password) {
      user.password = await this.hashPassword(user.password)
    }

    const result = await UserModel.updateOne({_id: user._id, ...notDeleted}, {$set: classToPlain(user)});
    return validateDatabaseUpdate(result, "User not found")
  }

  /**
   * Soft deletes an user.
   *
   * @param userId The ID of the user to be soft deleted
   * @returns `true` if the user has been successfully updated
   * @throws {@link RecordNotFoundError} if no user with the specified ID is found
   */
  async deleteUser(userId: string): Promise<boolean> {
    const result = await UserModel.updateOne({_id: userId, ...notDeleted}, {$set: {deleted: true}})
    return validateDatabaseUpdate(result, "User not found")
  }

  private async hashPassword(password: string) {
    return bcrypt.hash(password, 10);
  }
}
