import Controller from "../../models/controller";
import Module from "../../models/module";
import UserController from "./user.controller";
import UserService from "./user.service";

export default class UserModule implements Module {
  controllers: Controller[];

  constructor() {
    const userService = new UserService()
    this.controllers = [
        new UserController(userService)
    ]

    userService.initAdmin().then(user => {
      if (user) {
        console.log(`The first admin has been created:\nEmail: ${user.email}\nPassword: ${user.password}`)
      }
    })
  }
}
