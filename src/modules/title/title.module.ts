import Controller from "../../models/controller";
import Module from "../../models/module";
import TitleController from "./title.controller";
import TitleService from "./title.service";

export default class TitleModule implements Module {
  controllers: Controller[];

  constructor() {
    const titleService = new TitleService();
    this.controllers = [
        new TitleController(titleService)
    ];
  }
}
