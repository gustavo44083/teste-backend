import { v4 as uuidv4 } from "uuid"
import { TitleModel } from "../title.model";
import TitleService from "../title.service";

describe('The TitleService', () => {
  describe('when retrieving titles', () => {
    it('should retrieve all titles from the database', async () => {
      // create titles
      await TitleModel.create([
        {
          _id: uuidv4(),
          title: 'Test Title',
          year: '2019',
          genres: ['Action'],
          director: 'John Doe',
          cast: ["Jane Doe"],
          plot: '',
          createdAt: new Date()
        },
        {
          _id: uuidv4(),
          title: 'Test Title: The Test Returns',
          year: '2020',
          genres: ['Action'],
          director: 'John Doe',
          cast: ["Jane Doe"],
          plot: '',
          createdAt: new Date()
        }
      ])

      // retrieve titles
      const titleService = new TitleService()
      const titles = await titleService.getTitles(0, 10);
      expect(titles).toHaveLength(2)
    });
  });
});
