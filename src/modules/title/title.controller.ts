import { plainToClass } from 'class-transformer'
import { IsNumber, Max, Min, validate } from 'class-validator'
import { Request, Response, Router } from 'express'
import Controller from '../../models/controller'
import { RecordNotFoundError } from '../../models/exceptions'
import { checkRole } from '../../util'
import { User } from '../user/user.model'
import { Title } from './title.model'
import TitleService from './title.service'

export default class TitleController implements Controller {
  path = '/titles'
  router = Router()
  authenticated = true

  constructor(private titleService: TitleService) {
    this.router.get('/', this.getTitles)
    this.router.post('/', checkRole('admin'), this.createTitle)
    this.router.get('/:id', this.getTitle)
    this.router.put('/:id', checkRole('admin'), this.editTitle)
    this.router.delete('/:id', checkRole('admin'), this.deleteTitle)
    this.router.post('/:id/rate', this.rateTitle)
  }

  /**
   * Retrieves all registered and non-deleted titles.
   * Required roles: none
   */
  private getTitles = async (request: Request, response: Response) => {
    const titles = await this.titleService.getTitles(
        parseInt((request.query['page'] as string) || '0'),
        parseInt((request.query['limit'] as string) || '10')
    )

    response.json({titles})
  }

  /**
   * Retrieves a specific title.
   * Required roles: admin
   */
  private getTitle = async (request: Request, response: Response) => {
    const title = await this.titleService.getTitle(request.params['id']);
    if (title) {
      response.json(title)
    } else {
      response.status(404).send()
    }
  }

  /**
   * Registers a new title. The request body should include all the title's data.
   * Required roles: admin
   */
  private createTitle = async (request: Request, response: Response) => {
    const title = plainToClass(Title, request.body);
    const errors = await validate(title);
    if (errors.length) {
      return response.status(400).json({errors})
    }

    response.json(await this.titleService.createTitle(title))
  }

  /**
   * Updates data for an existing title. The request body should include all the title's data to be updated.
   * Required roles: admin
   */
  private editTitle = async (request: Request, response: Response) => {
    const titleData = plainToClass(Title, request.body);
    const errors = await validate(titleData)
    if(errors.length) {
      return response.status(400).json({errors})
    }

    titleData._id = request.params['id']
    try {
      await this.titleService.updateTitle(titleData)
      response.json(await this.titleService.getTitle(titleData._id))
    } catch (e) {
      if(!(e instanceof RecordNotFoundError))
        throw e

      response.status(404).send()
    }
  }

  /**
   * Deletes a title.
   * Required roles: admin
   */
  private deleteTitle = async (request: Request, response: Response) => {
    try {
      const deleted = await this.titleService.deleteTitle(request.params['id']);
      if (deleted) {
        return response.status(200).send()
      }
    } catch (e) {
      if(!(e instanceof RecordNotFoundError))
        throw e
    }

    response.status(404).send()
  }

  /**
   * Rates the title from 0 to 4.
   * Required roles: none
   */
  private rateTitle = async (request: Request, response: Response) => {
    const title = await this.titleService.getTitle(request.params['id']);
    if (!title) {
      return response.status(404).send()
    }

    const ratingRequest = plainToClass(TitleRatingRequest, request.body)
    const errors = await validate(ratingRequest)
    if (errors.length) {
      return response.status(400).json({errors})
    }

    const user = request.user as User
    await this.titleService.updateTitleRating(title, user, ratingRequest.rating)
    response.json(await this.titleService.getTitleRating(title, user))
  }
}

class TitleRatingRequest {
  @IsNumber()
  @Min(0)
  @Max(4)
  rating: number
}
