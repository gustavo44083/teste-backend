import { DocumentType } from '@typegoose/typegoose'
import { classToClass, classToPlain, plainToClass } from 'class-transformer'
import { v4 as uuidv4 } from 'uuid'
import { customToJson, notDeleted, validateDatabaseUpdate } from '../../util'
import { User } from '../user/user.model'
import { Title, TitleModel } from './title.model'
import { TitleRating, TitleRatingModel } from './titleRating.model'


export default class TitleService {
  /**
   *
   */
  async getTitles(page: number, limit: number): Promise<DocumentType<Title>[]> {
    return TitleModel.find(notDeleted).sort('-createdAt').limit(limit).skip(limit * page)
  }

  /**
   * Retrieves a title's data, including its average rating.
   *
   * @param id The title's ID
   */
  async getTitle(id: string): Promise<Title | null> {
    const results = await TitleModel.aggregate([
      {
        $match: {_id: id}
      },
      {
        $lookup: {
          from: 'titleratings',
          localField: '_id',
          foreignField: 'titleId',
          as: 'ratings'
        }
      },
      {
        $addFields: {
          rating: {$avg: ['$ratings.rating']}
        }
      },
      {
        $project: {
          ratings: 0
        }
      }
    ])

    if (!results.length) {
      return null;
    }

    return plainToClass(Title, customToJson(results[0], results[0]))
  }

  /**
   * Creates a new title and assigns it an unique ID.
   *
   * @param title The title's data
   * @returns The created title
   */
  async createTitle(title: Title): Promise<DocumentType<Title>> {
    title = classToClass(title)
    title._id = uuidv4()
    title.createdAt = new Date()
    return TitleModel.create(title)
  }

  /**
   * Updates an existing title's data. If a password is provided, it will be hashed before saving to the database.
   * The title object must have an ID.
   *
   * @param title The updated title's data
   * @returns `true` if the title has been updated
   * @throws {@link RecordNotFoundError} if no title with the specified ID is found
   */
  async updateTitle(title: Title): Promise<boolean> {
    if (!title._id) {
      throw new Error('Title must have an ID to be updated')
    }

    const result = await TitleModel.updateOne({_id: title._id, ...notDeleted}, {$set: classToPlain(title)})
    return validateDatabaseUpdate(result, 'Title not found')
  }

  /**
   * Soft deletes a title.
   *
   * @param titleId The ID of the title to be soft deleted
   * @returns `true` if the title has been successfully updated
   * @throws {@link RecordNotFoundError} if no title with the specified ID is found
   */
  async deleteTitle(titleId: string): Promise<boolean> {
    const result = await TitleModel.updateOne({_id: titleId, ...notDeleted}, {$set: {deleted: true}})
    return validateDatabaseUpdate(result, 'Title not found')
  }

  /**
   * Retrieves a user's rating for a title.
   *
   * @param title The rated title
   * @param user The user
   */
  async getTitleRating(title: Title, user: User): Promise<DocumentType<TitleRating> | null> {
    return TitleRatingModel.findOne({titleId: title._id, userId: user._id})
  }

  /**
   * Creates or updates a user's rating for a title.
   *
   * @param title The title to be rated
   * @param user The user rating the title
   * @param rating The user's rating for the title
   * @returns `true` if the title's rating has been successfully updated
   */
  async updateTitleRating(title: Title, user: User, rating: number): Promise<boolean> {
    const query = {titleId: title._id, userId: user._id}
    const result = await TitleRatingModel.updateOne(query, {$set: {rating}}, {upsert: true})
    return validateDatabaseUpdate(result, '')
  }
}
