import { getModelForClass, modelOptions, prop } from '@typegoose/typegoose'
import { Base } from "@typegoose/typegoose/lib/defaultClasses";
import { defaultModelOptions } from '../../util'

@modelOptions(defaultModelOptions)
export class TitleRating extends Base<string> {
  @prop()
  _id: string

  @prop()
  userId: string

  @prop()
  titleId: string

  @prop()
  rating: number
}

export const TitleRatingModel = getModelForClass(TitleRating)
