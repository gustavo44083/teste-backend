import { getModelForClass, modelOptions, prop } from "@typegoose/typegoose";
import { Base } from "@typegoose/typegoose/lib/defaultClasses";
import { Exclude } from 'class-transformer'
import { IsDefined, IsString } from "class-validator";
import { defaultModelOptions } from "../../util";

@modelOptions(defaultModelOptions)
export class Title extends Base<string> {
  @prop()
  _id: string

  @prop()
  @Exclude()
  createdAt: Date

  @prop()
  @IsString()
  title: string

  @prop()
  @IsDefined()
  year: string

  @prop({type: String})
  genres: string[]

  @prop()
  @IsString()
  director: string

  @prop({type: String})
  cast: string[]

  @prop()
  plot: string

  @prop({select: false})
  @Exclude()
  deleted?: boolean

  @Exclude()
  rating?: number
}

export const TitleModel = getModelForClass(Title)
