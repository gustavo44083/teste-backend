import "./lib"
import App from "./app";
import AuthModule from "./modules/auth/auth.module";
import TitleModule from "./modules/title/title.module";
import { mongoose } from "@typegoose/typegoose";
import { SETTINGS } from "./config";
import UserModule from "./modules/user/user.module";


(async () => {
  await mongoose.connect(SETTINGS.MONGODB_URI, {useNewUrlParser: true, useUnifiedTopology: true});
  const app = new App([
    new UserModule(),
    new AuthModule(),
    new TitleModule()
  ]);

  app.init({
    cors: SETTINGS.CORS_OPTIONS
  })

  app.listen(SETTINGS.PORT, () => {
    console.log(`Server listening on port ${SETTINGS.PORT}`)
  })
})();

