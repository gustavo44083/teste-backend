export const SETTINGS = {
  PORT: parseInt(process.env.PORT || '3000'),
  MONGODB_URI: process.env.MONGODB_URI || 'mongodb://localhost/imdb',
  CORS_OPTIONS: JSON.parse(process.env.APP_CORS_OPTIONS || '{}')
}
